package com.bradkarels.spark.simple

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object SuperSimple {
  def main(args: Array[String]) {
    // A file with wise words the 14th Dalai Lama - Tenzin Gyatso
    // Adjust the path to the following file to match your environment.
    val compassionFile = "/home/bkarels/tenzingyatso.txt" // Must exist on your local file system.
    val conf = new SparkConf().setAppName("Super Simple Spark App")

    val sc = new SparkContext(conf)
    val compassionLines = sc.textFile(compassionFile, 2).cache()
    val numPeace = compassionLines.filter(line => line.contains("peace")).count()
    val numLove = compassionLines.filter(line => line.contains("love")).count()
    println("Talks of peace: %s".format(numPeace))
    println("Speaks of love: %s".format(numLove))
  }
}